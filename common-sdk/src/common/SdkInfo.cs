namespace Coin.Sdk.Common
{
    public class SdkInfo
    {
        // NOTE: this version is maintained by the makefile, do not modify or move to another file
        public static string UserAgent = "coin-sdk-dotnet-0.0.5";
    }
}